import java.math.BigDecimal;

public class HW2 {

    public static void main(String[] args) {

        /*
        Задание 2
         */
        double pi = 3.141592653d;
        String second= "2.7182818284590452";

        System.out.println(pi);
        System.out.println(second);

        /*
        Задание 3
         */

        String string1 = "\nмоя строка 1";
        String string2 = "\tмоя строка 2";
//        String string3 = "\rмоя строка 3";

        System.out.println(string1);
        System.out.println(string2);
//        System.out.println(string3);
    }
}
